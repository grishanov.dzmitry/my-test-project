        const { studentId, taskId } = args;
        const taskService = container.get<TasksService>(TasksService);
        const studentService = container.get<StudentService>(StudentService);
        

        const { fileToCheckPath, acceptableSimilarity } = await taskService.getTaskById(
            taskId as string
        );
        
        const { courseId, projectId } = await studentService.getStudentById(studentId as string);
        const studentsOfCourse = await studentService.getStudentsByCourseId(
            courseId.toString()
        );
        const studentsWithoutCurrent = studentsOfCourse.filter((student) => student.id !== studentId);
        // console.log('22222', studentsWithoutCurrent[0]);
        
        const studentFile = await getRawFileFromRepository(
            projectId.toString(), fileToCheckPath
        );

        const files = await getRawFileFromAllRepositories(
            studentsWithoutCurrent.map(student => student.projectId),
            fileToCheckPath
        );
        const isSimilarityPassed = getIsSimilarityPassed(
            studentFile, files, acceptableSimilarity
        );
